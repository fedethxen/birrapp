<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    const CREATED_AT = 'fecha_alta';

    //  Indica que el campo "rol_id" de la tabla User hace referencia al id de la tabla roles por medio
    //  de los modelos
    
    public function rol(){
        return $this->belongsTo('App\Rol','rol_id');
    }
    public function favorito(){
        return $this->hasMany('App\Favorito','user_id');
    }
    public function img_cerv(){
        return $this->hasMany('App\ImagenUser', 'user_id');
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombres', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ]; 
}
