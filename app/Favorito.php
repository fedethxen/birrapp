<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Favorito extends Model
{
    protected $table = 'favoritos';
    public $timestamps = false;
    public function users(){
        return $this->belongsTo('App\User','user_id');        
    } 
    public function cervecerias(){
        return $this->belongsTo('App\Cerveceria','cerveceria_id');  
    }
}
