<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cerveceria extends Model
{
    protected $table = 'cervecerias';
    
    public function usuario_fav(){
        return $this->belongsToMany('App\User', 'favoritos');
    }
    public function users_cerv(){
        return $this->belongsToMany('App\User', 'cerveceria_user');
    }
    public function img_cerv(){
        return $this->hasMany('App\ImagenCerveceria', 'cerveceria_id');
    }
    public function ciudad(){
        return $this->belongsTo('App\Ciudad','ciudad_id');        
    }


    
}
