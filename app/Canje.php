<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Canje extends Model
{
    protected $table = 'canjes';
    const CREATED_AT = 'fecha_creacion';
    const UPDATED_AT = 'fecha_canje';
}
