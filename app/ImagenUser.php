<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImagenUser extends Model
{
    //
    protected $table = 'imgusers';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public function user(){
        return $this->belongsTo('App\User');
    }
}
