<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Favorito;

use App\Helpers\JwtAuth;

class FavoritoController extends Controller
{
	
    public function setFavorito(Request $request){
        $hash = $request->header('Authorization', null);
    	$jwtAuth = new JwtAuth();
    	$checkToken = $jwtAuth->checkToken($hash);

    	if($checkToken){

    		$json = $request->input('json',null);
    		$params = json_decode($json);
    		$params_array = json_decode($json,true);

    		$user = $jwtAuth->checkToken($hash, true);
			$rol = $user->rol_id;

			if($rol == 1 || $rol == 2 || $rol == 3 ){
				$validate = \Validator::make($params_array, [
					'cerveceria_id'=>'required',
					'user_id'=>'required',
				]);	

				if($validate->fails()){
					return response()->json($validate->errors(),400);
				}	
				
				$isset_fv = Favorito::where($params_array)->first();
				if(!isset($isset_fv)){
					$favorito = new Favorito();
				
					$favorito->user_id = $params->user_id;				
					$favorito->cerveceria_id = $params->cerveceria_id;	
					$favorito->save();
				
					$data = array(
						'favorito'=>$favorito,
						'status'=>'success',
						'code'=>200
					);					
				}else{
					$data = array(
						'message'=>'Ya ha marcado favorito',
						'status'=>'error',
						'code'=>400
					);
				}

				

			}else{
				$data = array(
					'message'=>'Usuario no autorizado',
					'status'=>'error',
					'code'=>400
				);
			}			
    	}else{
    		$data = array(
    			'message'=>'Login incorrecto',
    			'status'=>'error',
    			'code'=>400
    		);
    	}
		return response()->json($data,200);
	}	
	
	public function deleteFavorito(Request $request){
		$hash = $request->header('Authorization', null);
    	$jwtAuth = new JwtAuth();
    	$checkToken = $jwtAuth->checkToken($hash);

    	if($checkToken){

    		$user = $jwtAuth->checkToken($hash, true);
			$rol = $user->rol_id;

			$json = $request->input('json',null);		
			$params_array = json_decode($json,true);

			if($rol == 1 || $rol == 2 || $rol == 3 ){
				
				$validate = \Validator::make($params_array, [
					'cerveceria_id'=>'required',
					'user_id'=>'required',
				]);

				if($validate->fails()){
					return response()->json($validate->errors(),400);
				}
			
				$favorito = Favorito::where($params_array)->delete();
			
				if($favorito == 1){
					$data = array(
						'message'=>'Registro eliminado con exito',
						'status'=>'success',
						'code'=>200
					);
				}else{
					$data = array(
						'message'=>'El registro que desea eliminar no existe',
						'status'=>'error',
						'code'=>400
					);
				}				
			}else{
				$data = array(
					'message'=>'Usuario no autorizado',
					'status'=>'error',
					'code'=>400
				);
			}
		}else{
			$data = array(
				'message'=>'Login incorrecto',
				'status'=>'error',
				'code'=>400
			);
		}
		return response()->json($data,200);
	}
}
