<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Canje;
use App\Helpers\JwtAuth;

class CanjeController extends Controller
{
    public function setCanje(Request $request){
        $hash = $request->header('Authorization', null);
    	$jwtAuth = new JwtAuth();
    	$checkToken = $jwtAuth->checkToken($hash);

    	if($checkToken){            
            
    		$user = $jwtAuth->checkToken($hash, true);
			$rol = $user->rol_id;

			if($rol == 1 || $rol == 2 || $rol == 3 ){
			   
				$json = $request->input('json',null);            
    			$params = json_decode($json);
				$params_array = json_decode($json,true);
				
				// Usar validator todas las veces que se pueda
				
				$validate = \Validator::make($params_array, [
					'cerveceria_id'=>'required',
					'user_id'=>'required',
                ]);	
                
				if($validate->fails()){
					return response()->json($validate->errors(),400);
				}	
				
                // Buscar como trabajar con las fechas				
				$canje = new Canje();
				$canje->user_id = $params->user_id;				
				$canje->cerveceria_id = $params->cerveceria_id;
                $canje->codigo = rand(1,1000);
                $canje->fecha_expiracion = '2019-12-12';	
                $canje->estado = 'en_espera';
	
				$canje->save();
	
				$data = array(
					'canje'=>$canje,
					'status'=>'success',
					'code'=>200
				);
			}else{
				$data = array(
					'message'=>'Usuario no autorizado',
					'status'=>'error',
					'code'=>400
				);
			}			
    	}else{
    		$data = array(
    			'message'=>'Login incorrecto',
    			'status'=>'error',
    			'code'=>400
    		);
    	}
		return response()->json($data,200);
	}
	
	public function canjear($codigo,Request $request){
        $hash = $request->header('Authorization', null);
    	$jwtAuth = new JwtAuth();
    	$checkToken = $jwtAuth->checkToken($hash);

    	if($checkToken){
            
    		$userToken = $jwtAuth->checkToken($hash, true);
			$rol = $userToken->rol_id;

			if($rol == 4){
				$params_array = array(
					'estado'=>'canjeado'
				);
				//  Agregar condicion contra la fecha de expiracion
				// Tiene que chequear a que cerveceria pertenece
				$canje = Canje::where('codigo',$codigo)
								->where('estado','en_espera')
								->update($params_array);
				if($canje == 1){
					$data = array(
						'message'=>'Codigo canjeado con exito',
						'status'=>'success',
						'code'=>200
					);
				}else{
					$data = array(
						'message'=>'El codigo a canjear no existe o ya fue canjeado',
						'status'=>'error',
						'code'=>400
					);
				}	  
			}else{
				$data = array(
					'message'=>'Usuario no autorizado',
					'status'=>'error',
					'code'=>400
				);
			}			
    	}else{
    		$data = array(
    			'message'=>'Login incorrecto',
    			'status'=>'error',
    			'code'=>400
    		);
    	}
		return response()->json($data,200);
    }
}
