<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\ImagenUser;
use App\Helpers\JwtAuth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ImgUserController extends Controller
{
    //
    public function setImgUser($id, Request $request){
        $hash = $request->header('Authorization', null);
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);        
        if($checkToken){            
            /*$json 	= $request->input('json', null);
            $params = json_decode($json);
            $params_array = json_decode($json,true);*/
            
            $userToken = $jwtAuth->checkToken($hash, true);
            $rol = $userToken->rol_id;

            if($rol == 1){
                $validate = \Validator::make($request->all(),[
                    'image'=>'required|image',
                ]);
                
                if($validate->fails()){
                    return response()->json($validate->errors(),400);
                }  

                $imageUser = new ImagenUser();
            
                $imageUser->user_id = $id;
                $imageUser->nombre = $request->input('nombre');
                /*
                $image->direccion = $params->direccion;
                $image->latitud = $params->latitud;
                $image->longitud = $params->longitud;
                $image->telefono = $params->telefono;*/

                $image = $request->file('image');
                if($image){
                    $image_path = $image->getClientOriginalName();
                    Storage::disk('images')->put($image_path, \File::get($image));

                    $imageUser->image_path = $image_path;
                }


                $isset_image = ImagenUser::where('nombre','=',$imageUser->nombre)->first();
                if(!isset($isset_image)){
                    $imageUser->save();
                    //Elocuent
                    $data = array(
                        'status'=>'success',
                        'code'=>200,
                        'message'=>'Imagen de Usuario guardada correctamente'
                    );
                }else{
                    $data = array(
                        'status'=>'error',
                        'code'=>400,
                        'message'=>'Imagen de Usuario duplicada'
                    );
                }

            }else{
                $data = array(
                    'message'=>'Usuario no autorizado',
                    'status'=>'error',
                    'code'=>400
                );
            }			
        }else{
            $data = array(
                'message'=>'Login incorrecto',
                'status'=>'error',
                'code'=>400
            );
        }
        return response()->json($data,200);

    }

    public function getImage($image,Request $request ){
        //
        $hash = $request->header('Authorization', null);
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);        
        if($checkToken){   
            
            $exists = Storage::disk('images')->exists($image);
            if ($exists){
                return Storage::disk('images')->download($image);
            }else{
                $data = array(
                    'message'=>'No existe la imagen',
                    'status'=>'error',
                    'code'=>400
                ); 
            }
                
        }else{
            $data = array(
                'message'=>'Login incorrecto',
                'status'=>'error',
                'code'=>400
            );
        }
        return response()->json($data,200);

    }

    public function getImagesByUser($id,Request $request ){
        //Devuelve todos los "image_path" de un usuario
        
        $hash = $request->header('Authorization', null);
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);        
        if($checkToken){   

            $user=User::find($id);  
            //return $User->toArray();
    
            if (is_object($user)) {
                $user=User::find($id)->load('img_cerv'); 
                return response()->json(array(
                    'user'=>$user,
                    'status'=>'success'
                ),200);
            }else{
                return response()->json(array(
                    'message'=>'No existen datos de img del usuario',
                    'status'=>'error'
                ),400);
            }  
                        
        }else{
            $data = array(
                'message'=>'Login incorrecto',
                'status'=>'error',
                'code'=>400
            );
        }
        return response()->json($data,200);
    }
    
}
