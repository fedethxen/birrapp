<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Helpers\JwtAuth;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{	
	public function register(Request $request){
		$json 	= $request->input('json', null);
		$params = json_decode($json);
		 
		$rol_id = (!is_null($json) && isset($params->rol_id)) ? $params->rol_id : null;

		$email 	= (!is_null($json) && isset($params->email)) ? $params->email : null;

		$nombres 	= (!is_null($json) && isset($params->nombres)) ? $params->nombres : null;

		$apellidos 	= (!is_null($json) && isset($params->apellidos)) ? $params->apellidos : null;

		$dob 	= (!is_null($json) && isset($params->dob)) ? $params->dob : null;

		$password = (!is_null($json) && isset($params->password)) ? $params->password : null;


		if(!is_null($email) && !is_null($nombres) && !is_null($password) && !is_null($rol_id) && !is_null($apellidos) && !($rol_id == 2 && is_null($dob))){
			
			$user = new User();	

			$user->email 	 = $email;
			$user->rol_id 	 = $rol_id;
			$user->nombres 	 = $nombres;
			$user->apellidos = $apellidos;
			// Buscar un metodo para generar codigos aleatorios

			if($rol_id == 2){
				$user->cod_recomendacion = rand(1,1000);
				$user->dob 		 = $dob ;
			}

			$pwd = hash('sha256', $password);
			$user->password = $pwd;

			$isset_user = User::where('email','=',$email)->first();
			if(!isset($isset_user)){
				$user->save();

				//Elocuent
				$data = array(
					'status'=>'success',
					'code'=>200,
					'message'=>'Usuario registrado correctamente'
				);
			}else{
				$data = array(
					'status'=>'error',
					'code'=>400,
					'message'=>'Usuario duplicado'
				);
			}
		}else{
			$data = array(
				'status'=>'error',
				'code'=>400,
				'message'=>'Usuario no creado, algun campo nulo'
			);
		}
		return response()->json($data,200);
	}
	
	public function login(Request $request){
		$jwtAuth = new JwtAuth();

		//Recibir por post
		$json = $request->input('json', null);
		$params = json_decode($json);

		$email = (!is_null($json) && isset($params->email)) ? $params->email : null;
		$password = (!is_null($json) && isset($params->password)) ? $params->password : null;		
		$getToken = (!is_null($json) && isset($params->getToken)) ? $params->getToken : null;
		//Cifrar la password
		$pwd = hash('sha256', $password);
		
		if(!is_null($email) && !is_null($password) && ($getToken == null || $getToken == 'false' )){
			
			$signup = $jwtAuth->signup($email, $pwd);

		}elseif($getToken != null || $getToken != 'false'){
			
			$signup = $jwtAuth->signup($email, $pwd, $getToken);

		}else{
			$signup = array(
				'status'=>'error',
				'massage'=>'Envia tus datos por post'
			);
		}
		return response()->json($signup,200);
	}

	public function getFavoritos($id,Request $request){		
		$hash = $request->header('Authorization', null);
    	$jwtAuth = new JwtAuth();
    	$checkToken = $jwtAuth->checkToken($hash);

    	if($checkToken){

    		$userToken = $jwtAuth->checkToken($hash, true);
			$rol = $userToken->rol_id;

			if($rol == 1 || $rol == 2 || $rol == 3 ){				
				
				$user = User::find($id);   	
				if(isset($user)){
					$user = $user->load('favorito');
					
					$data = array(
						'favoritos'=>$user->favorito,
						'status'=>'success',
						'code'=>200
					);
				}else{
					$data = array(
						'message'=>'No existe un usuario con ese id',
						'status'=>'error',
						'code'=>400
					);
				}						
			}else{
				$data = array(
					'message'=>'Usuario no autorizado',
					'status'=>'error',
					'code'=>400
				);
			}
		}else{
			$data = array(
				'message'=>'Login incorrecto',
				'status'=>'error',
				'code'=>400
			);
		}
		return response()->json($data,200);		
	}
	
}
