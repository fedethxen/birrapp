<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ciudad;
use App\Helpers\JwtAuth;

class CiudadController extends Controller
{
    function setCiudad(Request $request){
        $hash = $request->header('Authorization', null);
    	$jwtAuth = new JwtAuth();
    	$checkToken = $jwtAuth->checkToken($hash);
    	if($checkToken){
            
            $user = $jwtAuth->checkToken($hash, true);
			$rol = $user->rol_id;

			if($rol == 1 ){
			   
				$json = $request->input('json',null);            
    			$params = json_decode($json);
                $params_array = json_decode($json,true);
								
                if(is_object($params)){
                    // Usar validator todas las veces que se pueda
                    $validate = \Validator::make($params_array, [
                        'cp'=>'required',
                        'nombre'=>'required',
                    ]);	
                    
                    if($validate->fails()){
                        return response()->json($validate->errors(),400);
                    }	
                    
                    $ciudad = new Ciudad();
                    $ciudad->cp = $params->cp;
                    $ciudad->nombre = $params->nombre;
                    
                    $ciudad->save();
                    $data = array(
                        'ciudad'=>$ciudad,
                        'status'=>'success',
                        'code'=>200
                    );

                }else{
                    $data = array(
                        'message'=>'Envie un objeto json por parametro',
                        'status'=>'error',
                        'code'=>400
                    );
                }
			}else{
				$data = array(
					'message'=>'Usuario no autorizado',
					'status'=>'error',
					'code'=>400
				);
			}
        }else{
    		$data = array(
    			'message'=>'Login incorrecto',
    			'status'=>'error',
    			'code'=>400
    		);
    	}
		return response()->json($data,200);
    }
    function getCiudades(Request $request){
        $hash = $request->header('Authorization', null);
    	$jwtAuth = new JwtAuth();
    	$checkToken = $jwtAuth->checkToken($hash);
    	if($checkToken){
            
            $user = $jwtAuth->checkToken($hash, true);
			$rol = $user->rol_id;

			if($rol == 1 || $rol == 2 || $rol == 3){
			   
                $ciudades = Ciudad::all();
                $data = array(
                    'ciudades'=>$ciudades,
                    'status'=>'success',
                    'code'=>200
                ); 
			}else{
				$data = array(
					'message'=>'Usuario no autorizado',
					'status'=>'error',
					'code'=>400
				);
			}
        }else{
    		$data = array(
    			'message'=>'Login incorrecto',
    			'status'=>'error',
    			'code'=>400
    		);
    	}
		return response()->json($data,200);
    }
    function getCiudad($id,Request $request){
        $hash = $request->header('Authorization', null);
    	$jwtAuth = new JwtAuth();
    	$checkToken = $jwtAuth->checkToken($hash);
    	if($checkToken){
            
            $user = $jwtAuth->checkToken($hash, true);
			$rol = $user->rol_id;

			if($rol == 1 || $rol == 2 || $rol == 3){
			   
                $ciudad = Ciudad::find($id);
                if(isset($ciudad)){
                    $ciudad->load('cervecerias');  
                    $data = array(
                        'ciudad'=>$ciudad,
                        'status'=>'success',
                        'code'=>200
                    );
                }else{
                    $data = array(
                        'message'=>'No existe una ciudad con ese id',
                        'status'=>'success',
                        'code'=>200
                    );
                }
                 
			}else{
				$data = array(
					'message'=>'Usuario no autorizado',
					'status'=>'error',
					'code'=>400
				);
			}
        }else{
    		$data = array(
    			'message'=>'Login incorrecto',
    			'status'=>'error',
    			'code'=>400
    		);
    	}
		return response()->json($data,200);
    }
    function deleteCiudad($id,Request $request){
        $hash = $request->header('Authorization', null);
    	$jwtAuth = new JwtAuth();
    	$checkToken = $jwtAuth->checkToken($hash);
    	if($checkToken){
            
            $user = $jwtAuth->checkToken($hash, true);
			$rol = $user->rol_id;

			if($rol == 1 || $rol == 2 || $rol == 3){
			   
                $ciudad = Ciudad::find($id);
                if(isset($ciudad)){
                    $ciudad->load('cervecerias');
                    if(sizeof($ciudad->cervecerias) == 0 ){
                        $ciudad->delete();
                        $data = array(
                            'ciudad'=>$ciudad,
                            'status'=>'success',
                            'code'=>200
                        );
                    }else{
                        $data = array(
                            'message'=>'El registro que desea eliminar posee registros asociados',
                            'status'=>'error',
                            'code'=>400
                        );
                    }                   
                }else{
                    $data = array(
                        'message'=>'El registro que desea eliminar no existe',
                        'status'=>'error',
                        'code'=>400
                    );
                }                 
			}else{
				$data = array(
					'message'=>'Usuario no autorizado',
					'status'=>'error',
					'code'=>400
				);
			}
        }else{
    		$data = array(
    			'message'=>'Login incorrecto',
    			'status'=>'error',
    			'code'=>400
    		);
    	}
		return response()->json($data,200);
    }

}
