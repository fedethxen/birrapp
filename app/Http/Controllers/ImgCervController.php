<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cerveceria;
use App\ImagenCerveceria;
use App\Helpers\JwtAuth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;


class ImgCervController extends Controller
{
    //
    public function setImgCerveceria($id, Request $request){
        $hash = $request->header('Authorization', null);
    	$jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);        
    	if($checkToken){            
            /*$json 	= $request->input('json', null);
		    $params = json_decode($json);
            $params_array = json_decode($json,true);*/
            
    		$userToken = $jwtAuth->checkToken($hash, true);
			$rol = $userToken->rol_id;

			if($rol == 1){
                $validate = \Validator::make($request->all(),[
					'image'=>'required|mimes:jpeg,bmp,png,gif,jpg',
				]);
               
				if($validate->fails()){
					return response()->json($validate->errors(),400);
				}  

                $imageCerv = new ImagenCerveceria();
          
                $imageCerv->cerveceria_id = $id;
                $imageCerv->nombre = $request->input('nombre');
                /*
                $image->direccion = $params->direccion;
                $image->latitud = $params->latitud;
                $image->longitud = $params->longitud;
                $image->telefono = $params->telefono;*/

                $image = $request->file('image');
                if($image){
                    $image_path = $image->getClientOriginalName();
                    Storage::disk('img_cervs')->put($image_path, \File::get($image));

                    $imageCerv->image_path = $image_path;
                }


                $isset_image = ImagenCerveceria::where('nombre','=',$imageCerv->nombre)->first();
                if(!isset($isset_image)){
                    $imageCerv->save();
                    //Elocuent
                    $data = array(
                        'status'=>'success',
                        'code'=>200,
                        'message'=>'Imagen de Cerveceria guardada correctamente'
                    );
                }else{
                    $data = array(
                        'status'=>'error',
                        'code'=>400,
                        'message'=>'Imagen de Cerveceria duplicada'
                    );
                }

			}else{
				$data = array(
					'message'=>'Usuario no autorizado',
					'status'=>'error',
					'code'=>400
				);
			}			
    	}else{
    		$data = array(
    			'message'=>'Login incorrecto',
    			'status'=>'error',
    			'code'=>400
    		);
    	}
		return response()->json($data,200);

    }

    public function getImage($image,Request $request ){
        //
        $hash = $request->header('Authorization', null);
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);        
        if($checkToken){   
            
            $exists = Storage::disk('img_cervs')->exists($image);
            if ($exists){
                return Storage::disk('img_cervs')->download($image);
            }else{
                $data = array(
                    'message'=>'No existe la imagen',
                    'status'=>'error',
                    'code'=>400
                ); 
            }
                
        }else{
            $data = array(
                'message'=>'Login incorrecto',
                'status'=>'error',
                'code'=>400
            );
        }
        return response()->json($data,200);

    }

    public function getImagesByCerv($id,Request $request ){
        //Devuelve todos los "image_path" de una cerveceria
        
        $hash = $request->header('Authorization', null);
        $jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);        
        if($checkToken){   

            $cerv=Cerveceria::find($id);  
            //return $cerv->toArray();
    
            if (is_object($cerv)) {
                $cerv=Cerveceria::find($id)->load('img_cerv'); 
                return response()->json(array(
                    'cerv'=>$cerv,
                    'status'=>'success'
                ),200);
            }else{
                return response()->json(array(
                    'message'=>'No existen datos de img de la cerveceria',
                    'status'=>'error'
                ),400);
            }  
                       
        }else{
            $data = array(
                'message'=>'Login incorrecto',
                'status'=>'error',
                'code'=>400
            );
        }
        return response()->json($data,200);

    }


}





