<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cerveceria;
use App\Helpers\JwtAuth;
use Illuminate\Support\Facades\DB;

class CerveceriaController extends Controller
{
    public function getCervecerias(Request $request){
        $hash = $request->header('Authorization', null);
    	$jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);        
    	if($checkToken){            
    		$userToken = $jwtAuth->checkToken($hash, true);
			$rol = $userToken->rol_id;

			if($rol == 1 || $rol == 2 || $rol == 3){
                $cervecerias = Cerveceria::all();
               // Parece innecesario que este metodo cargue las ciudades
               
                if(isset($cervecerias)){
                    $cervecerias->load('ciudad');                   
                    $data = array(
                        'cervecerias'=>$cervecerias,
                        'status'=>'success',
                        'code'=>200
                    );
                }else{
                    $data = array(
                        'message'=>	'No existen cervecerias',
                        'status' =>	'error',
                        'code'=>400
                    ); 
                }

			}else{
				$data = array(
					'message'=>'Usuario no autorizado',
					'status'=>'error',
					'code'=>400
				);
			}			
    	}else{
    		$data = array(
    			'message'=>'Login incorrecto',
    			'status'=>'error',
    			'code'=>400
    		);
    	}
		return response()->json($data,200);
    }
    
    
    public function setCerveceria(Request $request){
        $hash = $request->header('Authorization', null);
    	$jwtAuth = new JwtAuth();
        $checkToken = $jwtAuth->checkToken($hash);        
    	if($checkToken){            
            $json 	= $request->input('json', null);
		    $params = json_decode($json);
            $params_array = json_decode($json,true);
            
    		$userToken = $jwtAuth->checkToken($hash, true);
			$rol = $userToken->rol_id;

			if($rol == 1){
                $validate = \Validator::make($params_array,[
					'ciudad_id'=>'required',
                    'nombre'=>'required',
                    'direccion'=>'required',
                    'latitud'=>'required',
                    'longitud'=>'required',
                    'telefono'=>'required',
				]);
               
				if($validate->fails()){
					return response()->json($validate->errors(),400);
				}  

                $cerveceria = new Cerveceria();

                $cerveceria->ciudad_id = $params->ciudad_id;
                $cerveceria->nombre = $params->nombre;
                $cerveceria->direccion = $params->direccion;
                $cerveceria->latitud = $params->latitud;
                $cerveceria->longitud = $params->longitud;
                $cerveceria->telefono = $params->telefono;

                $isset_cerveceria = Cerveceria::where('nombre','=',$params->nombre)->first();
                if(!isset($isset_cerveceria)){
                    $cerveceria->save();
                    //Elocuent
                    $data = array(
                        'status'=>'success',
                        'code'=>200,
                        'message'=>'Cerveceria guardada correctamente'
                    );
                }else{
                    $data = array(
                        'status'=>'error',
                        'code'=>400,
                        'message'=>'Cerveceria duplicada'
                    );
                }

			}else{
				$data = array(
					'message'=>'Usuario no autorizado',
					'status'=>'error',
					'code'=>400
				);
			}			
    	}else{
    		$data = array(
    			'message'=>'Login incorrecto',
    			'status'=>'error',
    			'code'=>400
    		);
    	}
		return response()->json($data,200);

    }

    public function deleteCerveceria($id, Request $request){
        $hash = $request->header('Authorization', null);
    	$jwtAuth = new JwtAuth();
    	$checkToken = $jwtAuth->checkToken($hash);

    	if($checkToken){

    		$user = $jwtAuth->checkToken($hash, true);
			$rol = $user->rol_id;

			$json = $request->input('json',null);		
			$params_array = json_decode($json,true);

			if($rol == 1){	
                $cerveceria = Cerveceria::find($id);			
				if(isset($cerveceria)){
                    $cerveceria->delete();
					$data = array(
						'message'=>'Registro eliminado con exito',
						'status'=>'success',
						'code'=>200
					);
				}else{
					$data = array(
						'message'=>'El registro que desea eliminar no existe',
						'status'=>'error',
						'code'=>400
					);
				}				
			}else{
				$data = array(
					'message'=>'Usuario no autorizado',
					'status'=>'error',
					'code'=>400
				);
			}
		}else{
			$data = array(
				'message'=>'Login incorrecto',
				'status'=>'error',
				'code'=>400
			);
		}
		return response()->json($data,200);
    }

    public function getFavoritos(){

        $favoritos = Cerveceria::all();
       
        if(isset($favoritos)){
            $favoritos = $favoritos->load('usuario_fav');
            return response()->json(array(
                'favoritos' => $favoritos,
                'status' => 'success'
            ),200);
        }
        return response()->json(array(
            'message' => 'No existen cervecerias',
            'status' => 'error'
        ),400);
    }

    public function getUsers(){

        $users= Cerveceria::all();
       
        if(isset($users)){
            $users = $users->load('users_cerv');
            return response()->json(array(
                'users' => $users,
                'status' => 'success'
            ),200);
        }

        return response()->json(array(
            'message' => 'No existen cervecerias.',
            'status' => 'error'
        ),400);
    }

}
