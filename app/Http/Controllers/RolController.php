<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Rol;

class RolController extends Controller
{
	public function getRoles(){
		$roles = Rol::all();
		if(isset($roles)){
			return response()->json(array(
				'roles' => $roles,
				'status'=> 'success'  
			),200);
		}
		return response()->json(array(
			'message'=>	'No existen roles',
			'status' =>	'error'
		),400);
	}
	public function getRol($id){
		$rol = Rol::find($id);
		if(isset($rol)){
			$rol = $rol->load('users');
			return response()->json(array(
				'rol'=>$rol,
				'status'=>'success'
				),200); 
		}
        return response()->json(array(
            'message'=>'No existe un rol con ese id',
            'status'=>'error'
        ),400);
	}
}
