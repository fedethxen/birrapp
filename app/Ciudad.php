<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ciudad extends Model
{
    protected $table = 'ciudades';    
    public function cervecerias(){
        return $this->hasMany('App\Cerveceria','ciudad_id');
    }
}
