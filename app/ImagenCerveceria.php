<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImagenCerveceria extends Model
{
    //
    protected $table = 'imgcerveceria';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public function cerveceria(){
        return $this->belongsTo('App\Cerveceria');
    }
}
