<?php
namespace App\Helpers;

use Firebase\JWT\JWT;
use Firebase\Support\Facades\DB;
use App\User;

class JwtAuth{
	public $key;
	public function __construct(){
		$this->key = '/';
	}
	public function signup($email, $password, $getToken = null){
		$user = User::where(
			array(
				'email'=>$email,
				'password'=>$password
			)
		)->first();
		$signup = false;

		if(is_object($user)){
			$signup = true;
		}
		if($signup){
			$token = array(
				'sub'=> $user->id,
				'email'=> $user->email,
				'nombres'=> $user->nombres,
				'apellidos'=> $user->apellidos,
				'rol_id'=> $user->rol_id,
				'iat'=> time(),
				'exp'=> time() + (7*24*60*60)
			);

			$jwt = JWT::encode($token, $this->key,'HS256');

			$decode = JWT::decode($jwt, $this->key, array('HS256'));
			
			if(is_null($getToken)){
				return $decode;
			}else{
				return $jwt;
			}
		}else{
			return array(
				'status'=>'error',
				'message'=>'login ha fallado'
			);
		}
	}

	public function checkToken($jwt, $getIdentity = false){
		$auth = false;		
		try {
			$decode = JWT::decode($jwt, $this->key, array('HS256'));
		} catch (\UnexpectedValueException $e) {
			$auth = false;
		} catch (\DomainException $e) {
			$auth = false;
		}

		if(isset($decode) && is_object($decode) && isset($decode->sub)){
			$auth = true;
		}else{
			$auth = false;
		}
		if($getIdentity && $auth == true){
			return $decode;
		}
		return $auth;
	}
}