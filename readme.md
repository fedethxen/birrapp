<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, yet powerful, providing tools needed for large, robust applications.

## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of any modern web application framework, making it a breeze to get started learning the framework.

If you're not in the mood to read, [Laracasts](https://laracasts.com) contains over 1100 video tutorials on a range of topics including Laravel, modern PHP, unit testing, JavaScript, and more. Boost the skill level of yourself and your entire team by digging into our comprehensive video library.

## Consejos y Soluciones

* [Algunos Comandos GIT](#cambiar-de-repo-en-git)
* [Instalar projectos existentes Larave](Consideraciones-al-instalar-proyecto-LARAVEL-desde-GIT)
* [Solucion error de Rutas tras migracion](solucion-rutas-apache)
 

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).

## Cambiar de repo en GIT
esto eliminara todo lo relacionado a tu repositorio git:
```
rm -rf .git 
```

Inicilizas git en tu proyecto:
```
git init 
 ```

Agregar el nuevo repo:
```
git remote add origin  git@gitlab.com:fedethxen/laratest.git
```

Para volver a sincronizar el tracking del branch:
```
git pull git@gitlab.com:fedethxen/laratest master --rebase
```

**Deshacer lo viejo en local y traer version acualizada**

```
git fetch origin
git reset --hard origin/master
```
si quieres deshacer todos los cambios locales y commits, puedes traer la última versión del servidor y apuntar a tu copia local principal de esta forma



## Consideraciones al instalar proyecto LARAVEL desde GIT

**Permisos de escritura**

Según la documentación oficial de Laravel

    Después de instalar Laravel, tal vez debas configurar algunos permisos, Los directorios entre storage y la carpeta bootstrap/cache deben tener permisos de escritura por el servidor web.

Asegúrate de configurar esto correctamente usando

```	
sudo chmod -R 755 storage
```

**Instalando dependencias con Composer**

Lo primero que debes hacer luego de descargar un proyecto existente a tu maquina local y después de haber configurado tu virtualhost, es instalar las dependencias del proyecto con Composer.

Esto lo puedes hacer usando el siguiente comando en la consola, dentro de la carpeta raíz del proyecto:

```	
$ composer install
```	

De esta forma se instalarán todas las dependencias necesarias para el proyecto que fueron definidas en el archivo composer.json durante el desarrollo.


**Archivo de configuración de Laravel**

Cada nuevo proyecto con Laravel, por defecto tiene un archivo .env con los datos de configuración necesarios para el mismo, cuando utilizamos un sistema de control de versiones como git, este archivo se excluye del repositorio por medidas de seguridad .

Sin embargo  existe un archivo llamado .env.example que es un ejemplo de como crear un el archivo de configuración, podemos copiar este archivo desde la consola con:

```	
$ cp .env.example .env
```	

De esta forma ya tenemos el archivo de configuración de nuestro proyecto.

**Creando un nuevo API key**

Por medidas de seguridad cada proyecto de Laravel cuenta con una clave única que se crea en el archivo .env al iniciar el proyecto. En caso de que el desarrollador no te haya proporcionado están información, puedes generar una nueva API key desde la consola usando (posicionado dentro del directorio raiz del proyecto):

```	
$ php artisan key:generate
```	


**Mas Info desde:**

https://styde.net/como-instalar-proyectos-existentes-de-laravel/



## Solucion Rutas Apache :

**Modificar el archivo apache2.conf (ubuntu 16.04)**
```
sudo nano /etc/apache2/apache2.conf
```

```
<Directory />
        ...
        AllowOverride All
        ...
</Directory>

<Directory /usr/share>
        AllowOverride All
        ...
</Directory>

<Directory /var/www/>
        ...
        AllowOverride All
        ...
</Directory>
```


**Activar el modulo mod_rewrite con:**

```
sudo a2enmod rewrite
```

**Reiniciar servidor apache:**

```
sudo service apache2 restart
```

## Solucion al crear en storage el disk 'images' (en linux) :


**Dar todos los permisos (ver despues en inmplementacion...) y cambiar propietario a el user por default de apache : 'www-data', seguido reiniciar apache" (ubuntu 16.04)**

```
# chown -R www-data back-birrapp/
# chmod -R 777 back-birrapp/
# service apache2 restart

```

**Y si sale error de index es por que hay que limpiar cache:**

```
php artisan config:cache

php artisan cache:clear

```
(comandos dentro del directorio del proyecto)
