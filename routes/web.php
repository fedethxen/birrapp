<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});



// ---- Definir bien los nombres de los metodos de los controladores

/*
    --------------- ROL CONTROLLER --------------
*/
Route::get('/api/roles','RolController@getRoles');
Route::get('/api/rol/{id}','RolController@getRol');

/*
    --------------- USER CONTROLLER --------------
*/
Route::post('/api/register','UserController@register');
Route::post('/api/login','UserController@login');
Route::get('/api/user/{id}/favoritos','UserController@getFavoritos');

/*
    --------------- CERVECERIA CONTROLLER --------------
*/
Route::get('/api/cervecerias','CerveceriaController@getCervecerias');
Route::get('/api/cervecerias/fav','CerveceriaController@getFavoritos');
Route::get('/api/cervecerias/users','CerveceriaController@getUsers');
Route::delete('/api/cervecerias/{id}','CerveceriaController@deleteCerveceria');
Route::post('/api/cervecerias','CerveceriaController@setCerveceria');

/*
    --------------- FAVORITO CONTROLLER --------------
*/
Route::post('/api/favoritos','FavoritoController@setFavorito');
Route::delete('/api/favoritos','FavoritoController@deleteFavorito');

/*
    --------------- CANJE CONTROLLER --------------
*/
Route::post('/api/canjes','CanjeController@setCanje');
Route::put('/api/canjes/{codigo}','CanjeController@canjear');

/*
    --------------- CIUDAD CONTROLLER --------------
*/
Route::post('/api/ciudades','CiudadController@setCiudad');

/*
    --------------- IMAGEN CERVECERIA CONTROLLER --------------
*/
Route::post('/api/img/cervecerias/{id}','ImgCervController@setImgCerveceria');
Route::get('/api/img//cervecerias/{image}','ImgCervController@getImage');
Route::get('/api/img/cervecerias/{id}','ImgCervController@getImagesByCerv');

/*
    --------------- IMAGEN USUARIO CONTROLLER --------------
*/
Route::post('/api/img/users/{id}','ImgUserController@setImgUser');
Route::get('/api/img/users/{id}','ImgUserController@getImagesByUser');